-- SUMMARY --

Patterns Server is a module that extends Patterns allowing you to set up a "hub" for sharing pattern files.
Version 7.x-1.x uses the same 'patterns' table provided by Patterns module to store the information.
It relies on D2D to exchange the information in a distributed and secure way.

-- REQUIREMENTS --

* D2D: https://drupal.org/sandbox/shakty/1889370


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.
